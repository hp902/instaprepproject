package com.example.a7classeslocation

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.a7classeslocation.countrydisplay.CountryFragment
import com.example.a7classeslocation.mooddisplay.MoodsFragment

class HomeActivity : AppCompatActivity() {

    private lateinit var buttonCountry:Button
    private lateinit var buttonMood:Button
    private var alreadyExecuted:Boolean = false
    private val fragmentManager = supportFragmentManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        buttonCountry = findViewById(R.id.button_country)
        buttonMood = findViewById(R.id.button_mood)


        if(!alreadyExecuted) {
            callMoodsFragment()
            alreadyExecuted = true
        }

        callCountryFragment()
    }

    private fun callMoodsFragment() {
        /*val moodsFragment = MoodsFragment()
        buttonMood.setOnClickListener{
            //View.OnClickListener {
            moodsFragment.show(fragmentManager, "MoodsFragment_tag")
        }*/

        Handler(Looper.getMainLooper()).postDelayed({
            // Do something after 5s = 5000ms
            val moodsFragment = MoodsFragment()
            moodsFragment.show(fragmentManager, "MoodsFragment_tag")

        }, 2000)
    }

    private fun callCountryFragment(){
        val countryFragment = CountryFragment()
        buttonCountry.setOnClickListener{
            countryFragment.show(fragmentManager, "CountryFragment_tag")
        }
    }

}