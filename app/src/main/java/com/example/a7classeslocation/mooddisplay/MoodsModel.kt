package com.example.a7classeslocation.mooddisplay

import kotlin.properties.Delegates

class MoodsModel {


    var id by Delegates.notNull<Int>()
    lateinit var mood: String
    var img by Delegates.notNull<Int>()

    constructor(id: Int, mood: String, img:Int) {
        this.id = id
        this.mood = mood
        this.img = img
    }

    constructor()


}