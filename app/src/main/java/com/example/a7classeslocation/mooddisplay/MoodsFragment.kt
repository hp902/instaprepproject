package com.example.a7classeslocation.mooddisplay

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.*
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.a7classeslocation.R
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException


class MoodsFragment : DialogFragment(), MoodsAdapter.OnItemClickListener {

    private val getUrl = "https://staging.instapreps.com/api/moods/index"
    private val postUrl = "https://staging.instapreps.com/api/student_moods/create/"

    private val studentId:Int = 902



    private lateinit var recyclerView: RecyclerView
    private var moodsArrayList: ArrayList<MoodsModel> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val p = dialog!!.window!!.attributes
        p.windowAnimations = R.style.DialogAnimation

        val rootView = inflater.inflate(R.layout.moods_fragment, container)
        if (dialog != null && dialog?.window != null) {
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

        recyclerView = rootView.findViewById(R.id.moods_frag_listview) as RecyclerView
        val layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        recyclerView.layoutManager = layoutManager
        recyclerView.setHasFixedSize(true)

        getMoods()
        dialog!!.window!!.attributes = p

        return rootView
    }

    private fun getMoods() {
        //creating a string request to send request to the url
        val stringRequest = StringRequest(Request.Method.GET,getUrl, { response ->
                try {
                    val jsonArrayInfo = JSONArray(response)
                    for (i in 0 until jsonArrayInfo.length()) {
                        val jsonObjectDetail: JSONObject = jsonArrayInfo.getJSONObject(i)
                        val moodsModel = MoodsModel()
                        moodsModel.id = jsonObjectDetail.getInt("id")
                        moodsModel.mood = jsonObjectDetail.getString("mood")
                        when (moodsModel.mood) {
                            "Good" -> {
                                moodsModel.img = R.drawable.good_face
                            }
                            "Neutral" -> {
                                moodsModel.img = R.drawable.sleeping_face
                            }
                            else -> {
                                moodsModel.img = R.drawable.sad_face
                            }
                        }
                        moodsArrayList.add(moodsModel)
                    }

                    //stuff that updates ui
                    val adapter = MoodsAdapter(moodsArrayList, this, requireContext())
                    recyclerView.adapter = adapter


                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        ) { //displaying the error in toast if occurs
            Toast.makeText(requireContext(), "Error Occurred",Toast.LENGTH_SHORT).show()
        }

        //creating a request queue
        val requestQueue: RequestQueue = Volley.newRequestQueue(requireContext())
        requestQueue.add(stringRequest)
    }

    private fun postMoodsData(studentId: Int, moods_id: Int) {

        try {
            val requestQueue = Volley.newRequestQueue(requireContext())
            val jsonBody = JSONObject()
            jsonBody.put("student_detail_id", studentId)
            jsonBody.put("mood_id", moods_id)
            val mRequestBody = jsonBody.toString()
            val stringRequest: StringRequest =
                object : StringRequest(Method.POST, postUrl,
                    Response.Listener { response -> Log.i("LOG_VOLLEY", response!!) },
                    Response.ErrorListener { error -> Log.e("LOG_VOLLEY", error.toString()) }) {
                    override fun getBodyContentType(): String {
                        return "application/json; charset=utf-8"
                    }

                    @Throws(AuthFailureError::class)
                    override fun getBody(): ByteArray? {
                        return try {
                            mRequestBody.toByteArray()
                        } catch (uee: UnsupportedEncodingException) {
                            VolleyLog.wtf(
                                "Unsupported Encoding while trying to get the bytes of %s using %s",
                                mRequestBody,
                                "utf-8"
                            )
                            null
                        }
                    }

                    override fun parseNetworkResponse(response: NetworkResponse): Response<String> {
                        var responseString = ""
                        responseString = response.statusCode.toString()
                        return Response.success(
                            responseString,
                            HttpHeaderParser.parseCacheHeaders(response)
                        )
                    }
                }
            requestQueue.add(stringRequest)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    override fun onResume() {
        super.onResume()
        moodsArrayList.clear()
    }

    override fun onItemClick(position: Int) {

        val moodsModel:MoodsModel = moodsArrayList[position]
        postMoodsData(studentId, moodsModel.id)
        Toast.makeText(requireContext(), moodsModel.mood, Toast.LENGTH_SHORT).show()
        dialog?.dismiss()

    }
}