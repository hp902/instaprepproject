package com.example.a7classeslocation.mooddisplay

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.a7classeslocation.R

class MoodsAdapter(private val moodsArrayList: ArrayList<MoodsModel>, private val listener: OnItemClickListener, private val context: Context):RecyclerView.Adapter<MoodsAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.moods_adapter_layout, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        //holder.image.setImageResource(moodsArrayList[position].img)
        Glide.with(context).asGif().load(moodsArrayList[position].img).into(holder.image);
    }

    override fun getItemCount(): Int {
        return moodsArrayList.size
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val image:ImageView = itemView.findViewById(R.id.moods_image_view)

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position:Int = adapterPosition
            if(position !=   RecyclerView.NO_POSITION) {
                listener.onItemClick(position)
            }
        }
    }

    interface OnItemClickListener{
        fun onItemClick(position: Int)
    }
}