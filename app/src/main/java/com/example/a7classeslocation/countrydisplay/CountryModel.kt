package com.example.a7classeslocation.countrydisplay

import kotlin.properties.Delegates

class CountryModel {

    var id by Delegates.notNull<Int>()
    lateinit var code:String
    lateinit var country:String
    var publish by Delegates.notNull<Boolean>()

    constructor(id:Int,code:String,country:String,publish:Boolean){
        this.id = id
        this.code = code
        this.country = country
        this.publish = publish
    }

    constructor()

}