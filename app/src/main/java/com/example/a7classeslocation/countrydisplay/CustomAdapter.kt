package com.example.a7classeslocation.countrydisplay

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.LinearLayout
import android.widget.TextView
import com.example.a7classeslocation.R

class CustomAdapter(context:Context, arrayListDetails:ArrayList<CountryModel>) : BaseAdapter() {
    private val layoutInflater : LayoutInflater
    private val arrayListDetails:ArrayList<CountryModel>

    init {
        this.layoutInflater = LayoutInflater.from(context)
        this.arrayListDetails = arrayListDetails
    }

    override fun getCount(): Int {
        return arrayListDetails.size
    }

    override fun getItem(position: Int): Any {
        return arrayListDetails[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

        val view: View?
        val listRowHolder: ListRowHolder
        if (convertView == null){
            view = this.layoutInflater.inflate(R.layout.country_adapter_layout, parent, false)
            listRowHolder = ListRowHolder(view)
            view.tag = listRowHolder
        } else {
            view = convertView
            listRowHolder = view.tag as ListRowHolder
        }

        listRowHolder.id.text = arrayListDetails[position].id.toString()
        listRowHolder.code.text = arrayListDetails[position].code
        listRowHolder.country.text = arrayListDetails[position].country
        listRowHolder.publish.text = arrayListDetails[position].publish.toString()

        return view
    }


}

class ListRowHolder(row: View?) {
    val id:TextView
    val code:TextView
    val country:TextView
    val publish:TextView
    private val linearLayout: LinearLayout

    init {
        this.id = row?.findViewById(R.id.id) as TextView
        this.code = row?.findViewById(R.id.code) as TextView
        this.country = row?.findViewById(R.id.country) as TextView
        this.publish = row?.findViewById(R.id.publish) as TextView
        this.linearLayout = row?.findViewById(R.id.linearlayout) as LinearLayout

    }
}
