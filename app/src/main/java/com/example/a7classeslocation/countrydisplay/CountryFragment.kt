package com.example.a7classeslocation.countrydisplay

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.android.volley.RequestQueue
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.a7classeslocation.R
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject


class CountryFragment : DialogFragment() {

    private val getUrl = "https://instapreps.com/api/country_code"

    private lateinit var progress: ProgressBar
    private var arraylistCountry:ArrayList<CountryModel> = ArrayList()
    private lateinit var listView: ListView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val rootView = inflater.inflate(R.layout.country_fragment, container)

        if (dialog != null && dialog?.window != null) {
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        progress = rootView.findViewById(R.id.frag_progressbar)
        progress.visibility = View.VISIBLE

        listView = rootView.findViewById(R.id.country_frag_listview) as ListView
        getCountries()


        return rootView
    }

    private fun getCountries(){
        val stringRequest = StringRequest(com.android.volley.Request.Method.GET,getUrl, { response ->
            try {
                val jsonContact = JSONObject(response)

                val jsonArrayInfo: JSONArray = jsonContact.getJSONArray("data")

                val size:Int = jsonArrayInfo.length()
                //arraylistCountry= ArrayList()
                for (i in 0 until size) {
                    val jsonObjectDetail: JSONObject =jsonArrayInfo.getJSONObject(i)
                    val model = CountryModel()
                    model.id=jsonObjectDetail.getInt("id")
                    model.code=jsonObjectDetail.getString("code")
                    model.country=jsonObjectDetail.getString("country")
                    model.publish =jsonObjectDetail.getBoolean("publish")
                    arraylistCountry.add(model)
                }

                val objectAdapter = CustomAdapter(requireContext(), arraylistCountry)
                listView.adapter = objectAdapter
                progress.visibility = View.GONE


            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
        ) { //displaying the error in toast if occurs
            Toast.makeText(requireContext(), "Error Occurred", Toast.LENGTH_SHORT).show()
        }

        //creating a request queue
        val requestQueue: RequestQueue = Volley.newRequestQueue(requireContext())
        requestQueue.add(stringRequest)
    }
}